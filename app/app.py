from flask import Flask, jsonify, request
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))

@app.route('/')
def coe332():
    return jsonify(data)

@app.route('/instructors')
def get_instructors():
	return jsonify(data['instructors'])

@app.route('/instructors/<int:number>')
def get_instrutors_bynumber(number):
	if (number >= len(data['instructors'])):
		abort(404)
	return jsonify(data['instructors'][number])


@app.route('/assignments',methods=['GET'])
def get_assignments():
	return jsonify(data['assignments'])

@app.route('/assignments',methods=['POST'])
def post_assignments():
	request_data = request.data.decode("utf-8")
	request_dictionary = json.loads(request_data)
	return request_dictionary

@app.route('/meeting')
def get_meetings():
	return jsonify(data['meeting']) 
@app.route('/meeting/days')
def get_meeting_day():
	return jsonify(data['meeting']['day'])
def get_meeting_start():
	return jsonify(data['meeting']['start'])
@app.route('/meeting/end')
def get_meeting_end():
	return jsonify(data['meeting']['end'])
@app.route('/meeting/location')
def get_meeting_location():
	return jsonify(data['meeting']['location'])

